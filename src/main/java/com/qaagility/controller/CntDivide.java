package com.qaagility.controller;

public class CntDivide {

    public int divide(int value1, int value2) {
        if (value2 == 0) {
            return Integer.MAX_VALUE;
        }
        else {
            return value1 / value2;
        }
    }

}
