package com.qaagility.controller;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */

public class AboutTest {

    @Test
    public void descTest() {
        assertTrue("Description", new About().desc().contains("copyright notice"));
    }
}
