package com.qaagility.controller;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */

public class CalcmulTest {
    @Test
    public void mulTest() {
        assertEquals("Multiply", 18, new Calcmul().mul());
    }
}
