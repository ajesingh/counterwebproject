package com.qaagility.controller;


import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */

public class CalculatorTest {
    @Test
    public void addTest() {
        assertEquals("Add", 9, new Calculator().add());
    }
}
