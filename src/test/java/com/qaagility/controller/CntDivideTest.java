package com.qaagility.controller;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author <a href="mailto:AjeetKumar.Singh1@britebill.com">Ajeet</a>
 */
public class CntDivideTest {

    @Test
    public void dTest() {
        assertEquals("MaxValue", Integer.MAX_VALUE, new CntDivide().divide(1,0));
        assertEquals("Divide", 4, new CntDivide().divide(12,3));
    }
}
